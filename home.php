<?php

session_start();
if(!isset($_SESSION['username']))
{

    header('location: login.php');

}

if(isset($_GET['logout'])){
	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?php
    if(isset($_SESSION['username']))
    {?>
        
    <a href="home.php?logout='1'">Sign Out</a>

    <?php } ?>

</body>
</html>

