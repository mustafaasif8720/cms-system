<?php

session_start();

include('config.php');

$errors = array();

if($_SERVER['REQUEST_METHOD'] == "POST"){
    
   
    function sanitize_field($field){
        $field =  htmlspecialchars($field);
        $field =  strip_tags($field);
        $field =  trim($field);

        return $field;
    }
   
    $email = sanitize_field($_POST['email']);
    $password = sanitize_field($_POST['password']);
    $role = sanitize_field($_POST['role']);
  

    $uppercase = preg_match("@[A-Z]@", $password);
    $lowercase = preg_match("@[a-z]@", $password);
    $specialchar = preg_match("@[^\w]@", $password);
    $number = preg_match("@[0-9]@", $password);

 

    
if(!isset($email) || empty($email) || !isset($password)  || empty($password)  || !isset($role)  || empty($role) ){

    array_push($errors,"one or more fields are empty");

}
else{

    if(!filter_var($email,FILTER_VALIDATE_EMAIL)){
        
        array_push($errors, "Email not valid");

    }

    elseif(strlen($password) < 8  || !$uppercase || !$lowercase || !$specialchar || !$number){

        
        array_push($errors, "Password is not strong");
         
    }

    else{

    $query = "select * from users_table WHERE (email='$email')";

    $result = mysqli_query($con, $query);
    $user = mysqli_fetch_assoc($result);

    if($user){

        array_push($errors,"User already exists");
    }
    
    
    if(count($errors) == 0) {
   
    $sqli = "insert into users_table (email , password, role) VALUES ('$email','$password','$role')";

    mysqli_query($con, $sqli);

    $_SESSION['username'] = $email;
    header('location: home.php');
    
}
}
}
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

<div class="container">
    <div class="row justify-content-center">

<form method= "post" action= <?php echo htmlentities($_SERVER['PHP_SELF']); ?>>
    <input type="email" name="email" placeholder="Email" novalidate>

   

    <input type="password" name="password" placeholder="Password">

    <select name="role">
        <option value="admin">Admin</option>
        
        <option value="manager">Manager</option>
        
        <option value="employee">employee</option>
    </select>

    <input type="submit" name="submit">
    <?php
    if (count($errors) > 0) : ?>
   <div class="error-msg">
 <?php foreach($errors as $error) : ?>

 <p><?php echo $error; ?>

<?php endforeach; ?>
        <p></p>

    </div>
    <?php endif; ?>

    
</form>
</div>
</div>

</body>
</html>